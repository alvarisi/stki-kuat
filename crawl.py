__author__ = 'Wik'

import argparse
import requests
import threading

class Crawler:
    """Class untuk mengambil dokumen-dokumen HTML dari daftar link"""

    def __init__(self, links, outputDir, itemProcessingCallback=None):
        self.links = links
        self.outputDir = outputDir
        self.item_processing_callback = itemProcessingCallback

    #mengambil dokumen html dari list link yang disediakan (links)
    #lalu menyimpan dokumen html tersebut di direktori outputDir
    def start_crawl(self):
        for link in self.links:
            self.item_processing_callback() if self.item_processing_callback else None
            response = requests.get(link)
            if (response.status_code == 200):
                outputFile = open(self.outputDir + ('/' if self.outputDir[-1] != '/' else '') + link.split('/')[-1][:-1] + '.html', 'w')
                outputFile.write(response.text)
                outputFile.close()


if __name__ == '__main__':
    lock = threading.Lock()
    global counter
    global linkCount
    linkCount = 0
    counter = 0

    ###################################################################################################

    #mem-parse argumen-argumen command line dan mengembalikan objek
    #yang berisi argumen-argumen tersebut
    def parse_arguments():
        parser = argparse.ArgumentParser(description='Mengambil dokumen-dokumen html (crawling) \
                                                      berdasarkan link-link dari suatu file')
        parser.add_argument('inputFile', metavar='FILE',
                            help='File yang berisi link-link untuk di-crawl')
        parser.add_argument('outputDir', metavar='OUTDIR',
                            help='Direktori output')
        parser.add_argument('-t', metavar='T', dest='threadCount', default=1,
                            help='Jumlah thread yang ingin digunakan (default: 1)')
        return parser.parse_args()

    #membaca link-link dari suatu file
    #lalu mengembalikan list yang berisi link-link tersebut
    def read_links_from_file(inputFileName):
        inputFile = open(inputFileName, 'r')
        links = []
        for line in inputFile:
            links.append(line)
        return links

    #fungsi yang akan dipanggil ketika
    #objek crawler akan memproses suatu item (link)
    #digunakan untuk memberikan feedback kepada pengguna
    def itemProcessingCallback():
        global counter
        global linkCount
        lock.acquire()
        counter += 1
        print('\rMemproses..(' + str(counter) + '/' + str(linkCount) + ')')
        lock.release()

    #fungsi thread
    def threaded_crawl(links, outputDir, itemProcessingCallback):
        crawler = Crawler(links, outputDir, itemProcessingCallback)
        crawler.start_crawl()

    #########################################################################

    args = parse_arguments()
    links = read_links_from_file(args.inputFile)
    linkCount = len(links)
    threadCount = min(int(args.threadCount), linkCount)
    print("Jumlah link: " + str(linkCount))
    if linkCount > 0:
        linksPerThread = int(linkCount / threadCount)
        print(linksPerThread)
        linksModulo = linkCount % threadCount
        for i in range(threadCount):
            linksToProcess = links[i * linksPerThread : (i+1) * linksPerThread]
            thread = threading.Thread(target=threaded_crawl, args=(linksToProcess, args.outputDir, itemProcessingCallback))
            thread.start()
        if (linksModulo > 0):
            linksToProcess = links[args.threadCount * linksPerThread : ]
            thread = threading.Thread(target=threaded_crawl, args=(linksToProcess, args.outputDir, itemProcessingCallback))
            thread.start()
