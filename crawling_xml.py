import glob
import itertools
import xml.etree.elementTree as ET
from shutil import copytree


def crawlData(linkpath, pathfolder):
	file = open(linkpath, 'r')
	for link in file:
		for fname in glob.glob(pathfolder):
			with open(fname) as f:
		    		it = itertools.chain('<root>', f, '</root>')
				root = ET.fromstringlist(it)
				for linkresult in root.findall('link'):
					linkresult_text = linkresult.text
					if linkresult_text == link
						output_dir = "./" + link + "/"
						if not os.path.exists(output_dir):
							os.makedirs(output_dir)
						copyfile(pathfolder + fname, output_dir)
	file.close()


